package com.niudong.esdemo.Linux;

/**
 * https://www.cnblogs.com/gisblogs/p/3985393.html
 */
public interface ResourceUsage {
    public float get();
}
