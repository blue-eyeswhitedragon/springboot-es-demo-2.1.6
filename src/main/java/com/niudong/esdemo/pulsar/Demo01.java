package com.niudong.esdemo.pulsar;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;

public class Demo01 {

    public static void main(String[] args) throws PulsarClientException {
        PulsarClient client = PulsarClient.builder()
                .serviceUrl("pulsar://192.168.1.237:6650")
                .build();

        Producer<byte[]> producer = client.newProducer()
                .topic("my-topic")
                .create();

        // 然后你就可以发送消息到指定的broker 和topic上：

        //bin/pulsar-client consume my-topic -s "my-topic"
        for (int i = 0; i < 10; i++) {
            producer.send(("王杰王杰王杰王杰王杰"+i).getBytes());
        }

        producer.close();

        client.close();
    }
}
