package com.niudong.esdemo.pulsar;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.impl.schema.AvroSchema;

public class Test {



     public static void main(String[] args) throws Exception {
        PulsarClient pulsarClient = PulsarClient.builder().serviceUrl("pulsar://192.168.1.237:6650").build();
         Producer<Foo> producer = pulsarClient.newProducer(AvroSchema.of(Foo.class)).topic("test_topic").create();

         for (int i = 0; i < 1000; i++) {
            Foo foo = new Foo();
            foo.setField1(i);
            foo.setField2("foo" + i);
            foo.setField3(System.currentTimeMillis());
            producer.newMessage().value(foo).send();
        }
        producer.close();
        pulsarClient.close();
     }
}
class Foo {
    private int field1 = 1;
    private String field2;
    private long field3;

    public Foo(int field1, String field2, long field3) {
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
    }

    public Foo() {
    }

    public int getField1() {
        return field1;
    }

    public void setField1(int field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public long getField3() {
        return field3;
    }

    public void setField3(long field3) {
        this.field3 = field3;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "field1=" + field1 +
                ", field2='" + field2 + '\'' +
                ", field3=" + field3 +
                '}';
    }
}