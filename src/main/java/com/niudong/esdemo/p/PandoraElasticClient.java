package com.niudong.esdemo.p;

import java.io.IOException;
import java.net.InetAddress;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.IndexMetadata;
import org.elasticsearch.cluster.metadata.IndexTemplateMetadata;
import org.elasticsearch.cluster.metadata.MappingMetadata;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.compress.CompressedXContent;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.carrotsearch.hppc.cursors.ObjectCursor;

public class PandoraElasticClient {

        public static final String S_CLUSTER_NAME = "cluster.name";
        public static final String S_PANDORA = "pandora";
        public static final String S_TRANSPORT_ADDRESS = "192.168.1.213";

        public static void main(String[] args) throws IOException {

                Settings settings = Settings.builder().put(S_CLUSTER_NAME, S_PANDORA).build();
                TransportClient client = new PreBuiltTransportClient(settings)
                                .addTransportAddress(new TransportAddress(InetAddress.getByName(S_TRANSPORT_ADDRESS), 9300));


                ImmutableOpenMap<String, IndexMetadata> getIndices = client.admin().cluster().prepareState().execute().actionGet().getState().getMetadata()
                                .getIndices();

/*                ImmutableOpenMap<String, IndexTemplateMetadata> a = client.admin().cluster().prepareState().execute().actionGet().getState().getMetadata()
                                .getTemplates();*/
                                for (ObjectCursor<IndexMetadata> indexMetaData : getIndices.values()) {
                                        IndexMetadata indexData = indexMetaData.value;
                                        ImmutableOpenMap<String, MappingMetadata> mappings = indexData.getMappings();
                                        String mappingTem = mappings.get("app").source().toString();

                                        String iIndexName = indexData.getIndex().getName();
                                        System.out.println(iIndexName);
                                        System.out.println(mappingTem);
                                        System.out.println("------------------------------------");
                                }

        }
}