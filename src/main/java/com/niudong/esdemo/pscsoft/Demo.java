package com.niudong.esdemo.pscsoft;

import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.ScriptedMetricAggregationBuilder;

public class Demo {
    public static void main(String[] args) {
        ScriptedMetricAggregationBuilder aggregation = AggregationBuilders
                .scriptedMetric("agg")
                .initScript(new Script("state.heights = []"))
                .mapScript(new Script("state.heights.add(doc.gender.value == 'male' ? doc.height.value : -1.0 * doc.height.value)"))
                .combineScript(new Script("double heights_sum = 0.0; for (t in state.heights) { heights_sum += t } return heights_sum"))
                .reduceScript(new Script("double heights_sum = 0.0; for (a in states) { heights_sum += a } return heights_sum"));
    }
}
