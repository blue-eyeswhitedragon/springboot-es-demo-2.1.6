package com.niudong.esdemo.kafka;

import com.google.common.collect.Lists;

import java.util.Map;

public class Test {
    private static final String topics = "ez_raw,ez_async,ez_complete,ez_other,ez_statistical,ez_statistical";
    private static final String server = "192.168.1.191:9092";

    public static void main(String[] args) {
        Map<String, Long> monitor = KafkaMonitorTools.monitor(Lists.newArrayList(server), Lists.newArrayList(topics.split(",")));
        monitor.forEach((k, v)-> System.out.println("topic:" + k + " \tSummed Recent Offsets:" + v));
    }

}
