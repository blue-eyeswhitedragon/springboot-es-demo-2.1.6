package com.niudong.esdemo.Demo.IO.demo09_字符缓冲流;

import java.io.*;

//案例: 字符缓冲流入门.
public class Demo01 {
    public static void main(String[] args) throws IOException {
        //1. 通过字符缓冲输出流往文件中写一句话.
        /*BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/3.txt"));
        bw.write("夯哥是黑马最纯洁最有内涵的老师");
        bw.close();*/

        //2. 通过字符缓冲输入流读取文件中的内容.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/3.txt"));
        char[] chs = new char[5];
        int len = 0;
        while((len = br.read(chs)) != -1) {
            System.out.print(new String(chs, 0, len));
        }
        br.close();
    }
}
