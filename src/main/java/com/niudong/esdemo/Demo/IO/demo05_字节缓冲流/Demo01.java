package com.niudong.esdemo.Demo.IO.demo05_字节缓冲流;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

//案例: 高效的字节流入门
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //1. 通过字节缓冲输出流往文件中写一句话.
        //测试: public BufferedOutputStream(OutputStream os) 构造方法
        //分解版
       /* //1.1 创建普通的字节输出流.
        FileOutputStream fos = new FileOutputStream("./day05/data/1.txt");
        //1.2 创建高效的字节输出流, 关联普通的字节输出流.
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        //1.3 往文件中写数据.
        bos.write(97);
        bos.write(98);
        bos.write(99);
        //bos.flush();        //刷新, 即: 把缓冲区的数据刷出到文件中.
        //bos.close();
        //bos = null;
        bos.write(100);
        bos.write(101);
        bos.write(102);
        //bos.flush();
        //1.4 释放资源.
        bos.close();*/

        //合并版
       /* BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("./day05/data/1.txt"));
        byte[] bys = {65, 66, 67, 68, 69};
        bos.write(bys);
        //bos.flush();
        bos.close();*/

        //2. 通过字节缓冲输入流读取文件中的内容.
        //普通的字节输入流,  相当于: 没化妆之前的妹子.
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");
        //高效的字节输入流, 相当于: 化妆之后的妹子.
        BufferedInputStream bis = new BufferedInputStream(fis);
        //你 跟 妹子 在约会.
        int len = 0;
        while((len = bis.read()) != -1)
            System.out.println((char)len);
        //说再见.
        bis.close();        //把化妆之后的妹子给 Sha 了.
    }
}
