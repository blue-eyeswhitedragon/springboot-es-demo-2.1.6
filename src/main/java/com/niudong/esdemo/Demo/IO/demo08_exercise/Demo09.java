package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

//案例: 把ArrayList<Student>集合中的内容写入到文件中.
public class Demo09 {
    public static void main(String[] args) throws Exception {
        /*
            需求:
                1. 把ArrayList集合中的学生数据写入到文本文件。
                2. 要求：每一个学生对象的数据作为文件中的一行数据.
                3. 格式：学号,姓名,年龄,居住地
                4. 举例：itheima001,刘亦菲,33,北京
         */
        //1. 定义集合.
        ArrayList<Student> list = new ArrayList<>();
        //2. 往集合中添加元素.
        list.add(new Student("itheima001", "刘亦菲", 33, "北京"));
        list.add(new Student("itheima002", "赵丽颖", 31, "河北"));
        list.add(new Student("itheima003", "高圆圆", 35, "上海"));
        //3. 创建字符高效输出流, 用来往文件中写上.
        BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/4.txt"));
        //4. 遍历集合, 获取到每一个字符串.
        for (Student s : list) {
            //5. 把遍历到的字符串写入到文件中.
            bw.write(s.toString());
            //细节: 加入换行
            bw.newLine();
        }
        //6. 释放资源.
        bw.close();
    }
}
