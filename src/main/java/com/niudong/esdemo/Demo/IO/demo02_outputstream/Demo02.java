package com.niudong.esdemo.Demo.IO.demo02_outputstream;

import java.io.File;
import java.io.FileOutputStream;

//案例: FileOutputStream: 字节输出流, 三种写数据的方式.
/*
    FileOutputStream类的构造方法:
        public FileOutputStream(String path);   创建字节输出流对象, 关联目的地文件(字符串形式)
        public FileOutputStream(File path);     创建字节输出流对象, 关联目的地文件(File对象形式)

    FileOutputStream类的成员方法:
        public void write(int by);                          往目的地文件中写入: 一个字节.
        public void write(byte[] bys);                      往目的地文件中写入: 一个字节数组.
        public void write(byte[] bys, int start, int len);  往目的地文件中写入: 一个字节数组的 一部分.
        public void close();                                关闭流, 释放资源.
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1. 创建字节输出流对象, 关联目的地文件.
        //  ./ 表示当前项目的路径.
        //细节: 目的地文件不存在就创建, 如果存在就覆盖.
        //FileOutputStream fos = new FileOutputStream("./day05/data/2.txt");
        FileOutputStream fos = new FileOutputStream(new File("./day05/data/2.txt"));

        //2. 往目的地文件中写数据.
        //2.1 测试 public void write(int by);    往目的地文件中写入: 一个字节.
        /*fos.write(97);
        fos.write(98);
        fos.write(99);*/

        //2.2 测试public void write(byte[] bys); 往目的地文件中写入: 一个字节数组.
        /*byte[] bys = {65, 66, 67, 68, 69};      //ABCDE
        fos.write(bys);*/

        //2.3 测试 public void write(byte[] bys, int start, int length);  往目的地文件中写入: 一个字节数组的 一部分.
        byte[] bys = {65, 66, 67, 68, 69};       //ABCDE
        fos.write(bys,2 ,3);

        //3. 关闭字节输出流, 节约资源, 提高效率.
        fos.close();
    }
}
