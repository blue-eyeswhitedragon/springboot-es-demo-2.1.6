package com.niudong.esdemo.Demo.IO.demo04_exercise;

import java.io.FileInputStream;
import java.io.FileOutputStream;

//案例: 图片加密
//核心原理: 一个数字被同一个数字位异或两次, 该数字值不变.
//例如:  10 ^ 20 ^ 20 = 10
//例如: 5 ^ 10 ^ 5 = 10
public class Demo03 {
    public static void main(String[] args) throws Exception {
        /*System.out.println(10 ^ 20 ^ 20);
        System.out.println(5 ^ 10 ^ 5);*/
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("./day05/data/copy2.jpg");
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("./day05/data/copy3.jpg");
        //3. 定义变量, 记录读取到的字节内容(或者有效字节数).
        int len = 0;
        //4. 只要条件满足, 就一直读, 并将读取到的数据, 赋值给变量.
        while ((len = fis.read()) != -1) {
            //5. 将读取到的数据写入到指定的目的地文件中.
            //fos.write(len ^ 密钥值);
            fos.write(len ^ 8);
        }
        //6. 释放资源.
        fis.close();
        fos.close();
    }
}
