package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.*;

//案例一: 转换流拷贝文本文件.
//记忆: 转换流其实是 字符流.
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //需求: 把data/1.txt  -> 2.txt中.
        //方式1: 普通的字节流一次读写一个字节.
        //方式2: 普通的字节流一次读写一个字节数组.
        //方式3: 高效的字节流一次读写一个字节.
        //方式4: 高效的字节流一次读写一个字节数组.

        //方式5: 转换流一次读写一个字符.
        //核心思想: 字符流 = 字节流 + 编码表.
        //method01();

        //方式6: 转换流一次读写一个字符数组.
        //method02();
    }

    public static void method02() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        InputStreamReader isr = new InputStreamReader(new FileInputStream("./day05/data/1.txt"));
        //2. 创建输出流, 关联目的地文件.
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        char[] chs = new char[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = isr.read(chs)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            osw.write(chs, 0, len);
        }
        //6. 释放资源.
        isr.close();
        osw.close();
    }

    public static void method01() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        InputStreamReader isr = new InputStreamReader(new FileInputStream("./day05/data/1.txt"));
        //2. 创建输出流, 关联目的地文件.
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("./day05/data/2.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = isr.read()) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            osw.write(len);
        }
        //6. 释放资源.
        isr.close();
        osw.close();
    }
}
