package com.niudong.esdemo.Demo.IO.demo06_铺垫知识;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

//案例: 演示编解码 及 不同码表中文占用字节问题.
/*
    记忆:
        1. 编解码用的码表必须一致, 否则可能会乱码.
        2. 常用的编码表都有谁:

 */
public class Demo02 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //-28 -72 -83 -27 -101 -67  中国, UTF-码表.
        /*
          String类中的方法
            public byte[] getBytes();                   根据当前的默认码表, 将字符串转成其对应的字节数组.
            public byte[] getBytes(String charsetName); 根据指定的码表, 将字符串转成其对应的字节数组.
         */
        //编码: 字符串 -> 字节数组,  大白话: 我们能看懂的 ->  我们看不懂的.
        String s = "abc中国";
        //UTF-8码表的中文.
        byte[] bys1 = s.getBytes();                     //默认用的是UTF-8
        byte[] bys2 = s.getBytes("uTf-8");              //[97, 98, 99, -28, -72, -83, -27, -101, -67]
        System.out.println(Arrays.toString(bys1));
        System.out.println(Arrays.toString(bys2));
        System.out.println("--------------------");
        //GBK码表的中文.
        byte[] bys3 = s.getBytes("gbk");                //[97, 98, 99, -42, -48, -71, -6]
        System.out.println(Arrays.toString(bys3));
        System.out.println("--------------------");

        //解码: 字节数组 -> 字符串,  大白话: 我们看不懂的 -> 我们能看懂的.
        byte[] bys4 = {97, 98, 99, -42, -48, -71, -6};
        //String s2 = new String(bys4);                     //默认用的是UTF-8
        String s2 = new String(bys4,"gbk");                 //采用指定码表.
        System.out.println("s2: " + s2);
    }
}
