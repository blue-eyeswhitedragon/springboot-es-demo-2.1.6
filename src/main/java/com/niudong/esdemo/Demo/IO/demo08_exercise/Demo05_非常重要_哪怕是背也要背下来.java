package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

//案例: 字符高效流一次读写一行.
/*
    涉及到的方法:
        BufferedReader类中的特有方法:
            public String readLine();   //一次读取一行, 结束标记是"换行符", 并返回读取到的内容(不包括换行符), 读不到返回null
        BufferedWriter类中的特有方法:
            public void newLine();      //根据当前操作系统给出对应的换行符. windows(\r\n), linux(\n), mac(\r>
 */
public class Demo05_非常重要_哪怕是背也要背下来 {
    public static void main(String[] args) throws Exception {
        //1. 创建输入流, 关联数据源文件.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/1.txt"));
        //2. 创建输出流, 关联目的地文件.
        BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        String line = null;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((line = br.readLine()) != null) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bw.write(line);
            //核心细节: 别忘了加换行.
            //bw.write("\r\n");       ???
            bw.newLine();       //根据操作系统给出对应的换行符.
        }
        //6. 释放资源.
        br.close();
        bw.close();
    }
}
