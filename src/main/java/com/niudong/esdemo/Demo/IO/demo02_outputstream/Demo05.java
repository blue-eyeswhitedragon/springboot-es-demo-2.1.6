package com.niudong.esdemo.Demo.IO.demo02_outputstream;

import java.io.FileOutputStream;

//案例: 制造空文件.
public class Demo05 {
    public static void main(String[] args) throws Exception {
        //需求: 在指定的目录下, 创建一个大小为1G的 avi文件.
        //1. 创建字节输出流对象, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("d:/素人_周卓(高清无码)蓝光电影.avi");
        //2. 定义字节数组, 长度为: 1KB.
        byte[] bys = new byte[1024];        //1024个字节 = 1KB
        //3. 通过循环实现, 往文件中写入 1G的内容.
        for (int i = 0; i < 1024 * 1024; i++) {
            fos.write(bys);
        }
        //4. 关闭资源.
        fos.close();
    }
}
