package com.niudong.esdemo.Demo.IO.demo02_outputstream;

import java.io.File;
import java.io.FileOutputStream;

//案例: 演示如何换行以及追加数据.
/*
     FileOutputStream类的构造方法:
        public FileOutputStream(String path);                   创建字节输出流对象, 关联目的地文件(字符串形式)
        public FileOutputStream(String path, boolean append);   创建字节输出流对象, 关联目的地文件(字符串形式)
                                                                如果传入true, 表示往文件中追加数据.

        public FileOutputStream(File path);                     创建字节输出流对象, 关联目的地文件(File对象形式)
        public FileOutputStream(File path, boolean append);     创建字节输出流对象, 关联目的地文件(File对象形式)
                                                                如果传入true, 表示往文件中追加数据.

    FileOutputStream类的成员方法:
        public void write(int by);                          往目的地文件中写入: 一个字节.
        public void write(byte[] bys);                      往目的地文件中写入: 一个字节数组.
        public void write(byte[] bys, int start, int len);  往目的地文件中写入: 一个字节数组的 一部分.
        public void close();                                关闭流, 释放资源.

   结论:
        1. 不同的操作系统, 换行符不一样.
            \r\n    window操作系统
            \n      linux操作系统
            \r      mac操作系统.
        2. 在明天我们会学习一个方法 BufferedWriter#newLine(), 它的作用是根据当前的操作系统, 给出对应的换行符.
 */
public class Demo03 {
    public static void main(String[] args) throws Exception{
        //1. 创建字节输出流对象, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream(new File("./day05/data/2.txt"), true);

        //2. 往目的地文件中写数据.
        byte[] bys = {65, 66, 67, 68, 69};       //ABCDE
        //fos.write(写入换行符);
        //细节: String#getBytes(), 可以把字符串转换成其对应的字节数组形式.
        fos.write("\r\n".getBytes());
        fos.write(bys, 2, 3);                    //CDE

        //3. 关闭字节输出流, 节约资源, 提高效率.
        fos.close();
    }
}
