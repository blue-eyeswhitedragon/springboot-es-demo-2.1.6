package com.niudong.esdemo.Demo.IO.demo06_铺垫知识;

import java.io.FileInputStream;

//案例: 字节流读中文, 展示.
/*
    记忆:
        1. 字母, 数字, 特殊符号不管在什么码表中都是只占一个字节.
        2. 中文在GBK码表中占2个字节, 在UTF-8码表中占3个字节, 但是不管在什么码表中, 中文的第一个字节都是: 负数.
        3. 字符流 = 字节流 + 编码表.
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //1. 已知项目下的data文件夹下的1.txt文件中包含数据"abc中国".
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");
        //2. 请通过字节流一次读取一个字节的形式, 将上述文件中的内容读取出来.
        int len = 0;
        while ((len = fis.read()) != -1) {
            //3. 将上述读取到的数据打印到控制台上.
            System.out.print((byte)len + " ");      //-28 -72 -83 中,  -27 -101 -67 国
        }
        fis.close();
    }
}
