package com.niudong.esdemo.Demo.IO.demo04_exercise;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//案例一: 普通的字节流赋值文本文件.
/*
    IO流的核心步骤:
        1. 创建输入流, 关联数据源文件.
        2. 创建输出流, 关联目的地文件.
        3. 定义变量, 记录读取到的字节内容(或者有效字节数).
        4. 只要条件满足, 就一直读, 并将读取到的数据, 赋值给变量.
        5. 将读取到的数据写入到指定的目的地文件中.
        6. 释放资源.
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //方式一: 一次读写一个字节.
        //method1();

        //方式二: 一次读写一个字节数组.
        method2();

        System.out.println("拷贝完成!");
    }

    public static void method2() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("./day05/data/2.txt");
        //3. 定义变量, 记录读取到的字节内容(或者有效字节数).
        int len = 0;
        byte[] bys = new byte[3];       //这里写的值一般是: 1024的整数倍.
        //4. 只要条件满足, 就一直读, 并将读取到的数据, 赋值给变量.
        while ((len = fis.read(bys)) != -1) {
            //5. 将读取到的数据写入到指定的目的地文件中.
            fos.write(bys, 0, len);
        }
        //6. 释放资源.
        fis.close();
        fos.close();
    }

    public static void method1() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("./day05/data/2.txt");
        //3. 定义变量, 记录读取到的字节内容(或者有效字节数).
        int len = 0;
        //4. 只要条件满足, 就一直读, 并将读取到的数据, 赋值给变量.
        while ((len = fis.read()) != -1) {
            //5. 将读取到的数据写入到指定的目的地文件中.
            fos.write(len);
        }
        //6. 释放资源.
        fis.close();
        fos.close();
    }
}
