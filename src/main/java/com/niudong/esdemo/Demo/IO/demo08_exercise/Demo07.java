package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

//案例: 把文本文件中的数据读取到集合中，并遍历集合。要求：文件中每一行数据是一个集合元素
public class Demo07 {
    public static void main(String[] args) throws Exception {
        //十大开发原则其中之一: 对于一些比较消耗系统资源的对象(例如: IO流对象), 使用原则: 晚开早关.

        //1. 创建集合对象, 用来存储字符串.
        ArrayList<String> list = new ArrayList<>();
        //2. 创建字符高效输入流, 用来读取数据源文件.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的数据(字符串)
        String line = null;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的数据赋值给变量.
        while ((line = br.readLine()) != null) {
            //5. 将读取到的数据写入到集合中.
            list.add(line);
        }
        //6. 关流.
        br.close();
        //7. 遍历集合.
        for (String s : list) {
            System.out.println(s);
        }
    }
}
