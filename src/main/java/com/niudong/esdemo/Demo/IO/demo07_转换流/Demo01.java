package com.niudong.esdemo.Demo.IO.demo07_转换流;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

//案例: 转换流_入门.
/*
    OutputStreamWriter: 字符流 通向 字节流的桥梁.
        构造方法:
            public OutputStreamWriter(OutputStream os)                      //创建转换输出流对象, 采用默认码表(utf-8)往文件中写数据.
            public OutputStreamWriter(OutputStream os, String charsetName)  //创建转换输出流对象, 采用指定码表往文件中写数据.

    InputStreamReader:  字节流 通向 字符流的桥梁.
        构造方法:
            public InputStreamReader(InputStream is)                      //创建转换输入流对象, 采用默认码表(utf-8)从文件中读数据.
            public InputStreamReader(InputStream is, String charsetName)  //创建转换输入流对象, 采用指定码表从文件中读数据.

 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //核心思想: 字符流 = 字节流 + 编码表.
        //1. 演示OutputStreamWriter往文件中一次写入一个字符.
        //OutputStreamWriter osw = new OutputStreamWriter(字节输出流对象, 编码表);
       /* OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("./day05/data/1.txt"));
        osw.write('中');
        osw.write('国');
        osw.write('加');
        osw.write('油');
        osw.write('!');
        osw.close();*/

        //2. 演示InputStreamReader从文件中一次读取一个字符.
        InputStreamReader isr = new InputStreamReader(new FileInputStream("./day05/data/1.txt"));
        //一次读一个字符.
        int len = 0;
        while((len = isr.read()) != -1) {
            System.out.print((char)len);
        }
        isr.close();
    }
}
