package com.niudong.esdemo.Demo.IO.demo05_字节缓冲流;

import java.io.*;

//案例: 测试四种拷贝(视频)文件的效率
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //获取当前时间的毫秒值.
        long start = System.currentTimeMillis();
        //方式一: 普通的字节流一次读写一个字节.                4
        //method01();                                          //52102 毫秒.

        //方式二: 普通的字节流一次读写一个字节数组.            2
        //method02();                                          //94 毫秒

        //方式三: 高效的字节流一次读写一个字节.                3
        //method03();                                          //359毫秒


        //思考: 普通的字节流一次读写一个字节数组, 我们自定义的数组长度是: 8192个字节, 问: 这种方式和
        //高效的字节流一次读写一个字节, 谁的效率高?
        //结论: 普通的字节流一次读写一个字节数组 > 高效的字节流一次读写一个字节

        //方式四: 高效的字节流一次读写一个字节数组.            1
        //method04();                                          //28毫秒

        long end = System.currentTimeMillis();
        System.out.println("程序执行了: " + (end - start) + "毫秒");
    }

    public static void method04() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("");
        BufferedInputStream bis = new BufferedInputStream(fis);
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        byte[] bys = new byte[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = bis.read(bys)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bos.write(bys, 0, len);
            bos.flush();
        }
        //6. 释放资源.
        bis.close();
        bos.close();
    }

    public static void method03() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("");
        BufferedInputStream bis = new BufferedInputStream(fis);
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = bis.read()) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bos.write(len);
        }
        //6. 释放资源.
        bis.close();
        bos.close();
    }

    public static void method02() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("");
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("");
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        byte[] bys = new byte[8192];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = fis.read(bys)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            fos.write(bys, 0, len);
        }
        //6. 释放资源.
        fis.close();
        fos.close();
    }

    public static void method01() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("");
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("");
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = fis.read()) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            fos.write(len);
        }
        //6. 释放资源.
        fis.close();
        fos.close();
    }
}
