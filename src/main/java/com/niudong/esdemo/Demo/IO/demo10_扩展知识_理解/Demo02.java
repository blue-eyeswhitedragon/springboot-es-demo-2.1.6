package com.niudong.esdemo.Demo.IO.demo10_扩展知识_理解;

import java.io.*;
import java.util.Scanner;

//补充知识二: 递归赋值文件夹.
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1. 获取数据源文件夹路径.
        File src = getDir();
        //2. 获取目的地文件夹路径
        File dest = getDir();

        //3. 拷贝文件夹.
        copyDirectory(src, dest);
    }


    //1. 定义方法, 获取文件夹路径.
    public static File getDir() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("请录入一个文件夹路径: ");
            File dir = new File(sc.nextLine());
            if (dir.isDirectory())
                //是文件夹
                return dir;
            else
                System.out.println("您录入的路径不合法, 请重新录入: ");
        }
    }

    //2. 定义方法, 拷贝文件夹.

    /**
     * 拷贝文件夹
     *
     * @param src  数据源文件夹
     * @param dest 目的地文件夹
     */
    public static void copyDirectory(File src, File dest) throws Exception {
        /*
            例如:
                src:   D:\abc\src
                dest:  D:\abc\dest

                新目录: D:\abc\dest\src
         */
        //1. 在目的地文件夹下创建一个 和数据源文件夹 名字一模一样的文件夹.
        File newDir = new File(dest, src.getName());    // D:\abc\dest\    src
        if (!newDir.exists())
            newDir.mkdir();

        //2. 获取数据源数据文件夹下所有的 文件或者文件夹对象.   compile文件夹,  文件: DeadLock.java, pet.txt, Worker.java
        File[] listFiles = src.listFiles();
        //3. 遍历, 获取到数据源文件夹下, 每一个文件或者文件夹对象.
        for (File listFile : listFiles) {
            //listFile就是src目录下的: 每一个文件, 或者文件夹对象.
            //4. 判断, 如果是文件, 就拷贝.   listFile文件, 例如: Worker.java ->    newDir文件夹下.
            if (listFile.isFile()) {
                //细节: 用字节流,可以拷贝任意类型的文件.
                //标准的IO的复制代码.
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(listFile));

                //细节: 封装目的地文件路径.
                //目的地文件路径 + 数据源文件名
                File file = new File(newDir, listFile.getName());
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                int len = 0;
                while((len = bis.read()) != -1) {
                    bos.write(len);
                }
                bis.close();
                bos.close();
            } else {
                //5. 如果是文件夹, 就递归.   listFile文件夹, 例如: compile文件夹 ->  newDir文件夹下.
                copyDirectory(listFile, newDir);
            }

        }

    }
}
