package com.niudong.esdemo.Demo.IO.demo03_inputstream;

import java.io.FileInputStream;
import java.util.Arrays;

//案例: 字节输入流一次读取一个字节数组.
/*
    FileInputStream:
        概述:
            表示普通的字节输入流, 它是一个具体的类.
        成员方法:
            public int read();             从数据源文件中读取数据, 一次读取一个字节. 并返回该字节的ASCII码值, 读不到返回-1.
            public int read(byte[] bys)    从数据源文件中读取数据, 一次读取一个字节数组. 并将读取到的内容存储到字节数组中,
                                           然后返回 读取到的有效字节数, 读不到返回-1.
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1. 创建字节输入流对象, 关联数据源文件.
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");

        //2. 从数据源中读取数据.
        /*//方式一: 普通版.
        //定义一个长度为3的字节数组, 记录读取到的数据
        byte[] bys = new byte[3];                   //0, 0, 0
        //第一次读取, 读取3个字节.
        int len1 = fis.read(bys);
        System.out.println("len1: " + len1);        //3
        System.out.println(Arrays.toString(bys));   //a,b,c
        System.out.println("---------------------");
        //第二次读取, 读取3个字节.
        int len2 = fis.read(bys);
        System.out.println("len2: " + len2);        //3
        System.out.println(Arrays.toString(bys));   //d,e,f
        System.out.println("---------------------");
        //第三次读取, 读取3个字节.
        int len3 = fis.read(bys);
        System.out.println("len3: " + len3);        //1
        System.out.println(Arrays.toString(bys));   //g,e,f
        System.out.println("---------------------");
        //第四次读取, 读取3个字节.
        int len4 = fis.read(bys);
        System.out.println("len4: " + len4);        //-1
        System.out.println(Arrays.toString(bys));   //g,e,f
        System.out.println("---------------------");
*/
        //方式二: 实际开发中写法.
        //2.1 定义直接数组用来存储读取到的内容(字节)
        byte[] bys2 = new byte[5];
        //2.2 定义变量, 记录读取到的有效字节数.
        int len = 0;
        //2.3 循环读取, 只要条件满足, 就一直读, 并将读取到的有效字节数赋值给变量.
        while ((len = fis.read(bys2)) != -1) {
            //2.4 核心: 将读取到的数据打印到控制台上.
            //String s = new String(bys2);      //不能这样写, 如果最后不够3个字节, 会多一些数据.
            String s = new String(bys2, 0, len);
            System.out.print(s);
        }

        //3. 释放资源.
        fis.close();
        ;
    }
}
