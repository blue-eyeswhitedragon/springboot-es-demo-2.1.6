package com.niudong.esdemo.Demo.IO.demo03_inputstream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

//案例: FileInputStream入门.
/*
    InputStream: 字节输入流的底层抽象类, 因为是抽象类, 所以不能直接创建对象.
        它的常用的两个子类主要是:
            FileInputStream:  普通的字节输入流
            BufferedInputStream: 高效的字节输入流.

    FileInputStream:
        概述:
            表示普通的字节输入流, 它是一个具体的类.
        构造方法:
            public FileInputStream(String path);    创建字节输入流对象, 关联数据源文件(字符串形式)
            public FileInputStream(File path);      创建字节输入流对象, 关联数据源文件(File对象形式)
        成员方法:
            public int read();                      从数据源文件中读取数据, 一次读取一个字节. 并返回该字节的ASCII码值, 读不到返回-1.
            public void close();                    关闭流, 释放资源.
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //1. 创建字节输入流对象, 关联数据源文件.
        FileInputStream fis = new FileInputStream("./day05/data/1.txt");

        //2. 从数据源中读取数据.
        //方式一: 繁琐版本.
        //一次读取一个字节, 第一次读取
        /*int by1 = fis.read();
        //System.out.println("by1: " + ((char)by1));
        System.out.println("by1: " + by1);
        //一次读取一个字节, 第二次读取
        System.out.println("by2: " + fis.read());
        //一次读取一个字节, 第三次读取
        System.out.println("by3: " + fis.read());
        //一次读取一个字节, 第四次读取
        System.out.println("by4: " + fis.read());*/


        //方式二: 优化版, 采用循环优化, 因为不知道循环次数, 所以用while循环.
        /*//2.1 定义变量, 记录读取到的内容.
        int len = 0;
        //2.2 循环读取, 只要结果不为-1, 就一直读.
        while (len != -1) {
            //2.3 将读取到的数据, 赋值给变量.
            len = fis.read();
            //2.4 打印结果.
            System.out.println(len);
        }*/

        //方式三: 实际开发中写法.
        //2.1 定义变量, 记录读取到的内容.
        int len = 0;
        //2.2 循环读取数据, 并将读取到的数据赋值给变量, 只要条件满足(不等于-1)就一直读.
        /*
            (len = fis.read()) != -1 这行代码做了3件事儿:
                1. 执行 fis.read(),                   从数据源文件中 读取一个字节.
                2. 执行 len = fis.read()              将从数据源文件中读取到的字节, 赋值给变量.
                3. 执行 (len = fis.read()) != -1      判断读取到的内容是否是-1, 如果是说明文件中内容读完了, 程序结束
                                                      否则说明文件中还有数据, 程序接着读.
         */
        while ((len = fis.read()) != -1) {
            //2.3 打印结果.
            System.out.println(len);
        }
        //3. 释放资源.
        fis.close();
        ;
    }
}
