package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

//案例: 把data文件夹下的1.txt(UTF-8码表)文件内容 复制到 2.txt(GBK码表)文件中.
public class Demo03 {
    public static void main(String[] args) throws Exception {
        //1. 创建输入流, 关联数据源文件.
        InputStreamReader isr = new InputStreamReader(new FileInputStream("./day05/data/1.txt"),"utf-8");
        //2. 创建输出流, 关联目的地文件.
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("./day05/data/2.txt"),"gbk");
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        char[] chs = new char[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = isr.read(chs)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            osw.write(chs, 0, len);
        }
        //6. 释放资源.
        isr.close();
        osw.close();
    }
}
