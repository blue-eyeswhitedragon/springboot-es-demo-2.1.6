package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

//案例: 把ArrayList<String>集合中的内容写入到文件中.
public class Demo06 {
    public static void main(String[] args) throws IOException {
        //需求: 把ArrayList集合中的字符串数据写入到文本文件。要求：每一个字符串元素作为文件中的一行数据
        //1. 定义集合.
        ArrayList<String> list = new ArrayList<>();
        //2. 往集合中添加元素.
        list.add("王杰2");
        list.add("王杰1");
        list.add("王杰");
        //3. 创建字符高效输出流, 用来往文件中写上.
        BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/3.txt"));
        //4. 遍历集合, 获取到每一个字符串.
        for (String s : list) {
            //5. 把遍历到的字符串写入到文件中.
            bw.write(s);
            //细节: 加入换行
            bw.newLine();
        }
        //6. 释放资源.
        bw.close();
    }
}
