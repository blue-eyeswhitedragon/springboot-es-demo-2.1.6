package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.*;

//案例: 复制文件.
//代码内容: 高效字符流一次一个字符,  高效字符流一次一个字符数组.
//上述的两种方式只要做到 理解即可, 实际开发用的非常少.
public class Demo04 {
    public static void main(String[] args) throws Exception {
        //方式1: 高效字符流一次读写一个字符.
        //method01();

        //方式2: 高效字符流一次读写一个字符数组.
        //method02();
    }

    public static void method02() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/1.txt"));
        //2. 创建输出流, 关联目的地文件.
        BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        char[] chs = new char[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = br.read(chs)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bw.write(chs, 0, len);
        }
        //6. 释放资源.
        br.close();
        bw.close();
    }

    public static void method01() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/1.txt"));
        //2. 创建输出流, 关联目的地文件.
        BufferedWriter bw = new BufferedWriter(new FileWriter("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = br.read()) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bw.write(len);
        }
        //6. 释放资源.
        br.close();
        bw.close();
    }
}
