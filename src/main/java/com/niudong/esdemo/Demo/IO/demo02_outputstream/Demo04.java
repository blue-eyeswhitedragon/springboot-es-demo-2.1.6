package com.niudong.esdemo.Demo.IO.demo02_outputstream;

import java.io.FileOutputStream;

//案例: FileOutputStream, 加入异常处理.
public class Demo04 {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            //1. 创建字节输出流对象, 关联目的地文件.
            System.out.println(1 / 0);
            fos = new FileOutputStream("./day05/data/2.txt");
            //2. 往目的地文件中写数据.
            fos.write("hello world!".getBytes());
        } catch (Exception e) {
            //出现异常后的解决方案
            e.printStackTrace();
        } finally {
            //3. 关闭字节输出流, 节约资源, 提高效率.
            try {
                if (fos != null) {
                    fos.close();
                    fos = null;     //GC会优先回收null对象.
                }
                System.out.println("看看我执行了吗? ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
