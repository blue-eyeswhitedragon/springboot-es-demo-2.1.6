package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//案例二: 普通的字符流拷贝文本文件.
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //方式一: 普通的字符流一次读写一个字符.
        //method01();

        //方式二: 普通的字符流一次读写一个字符数组.
        //method02();
    }

    public static void method02() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileReader fr = new FileReader("./day05/data/1.txt");
        //2. 创建输出流, 关联目的地文件.
        FileWriter fw = new FileWriter("./day05/data/3.txt");
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        char[] chs = new char[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = fr.read(chs)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            fw.write(chs, 0, len);
        }
        //6. 释放资源.
        fr.close();
        fw.close();
    }

    public static void method01() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileReader fr = new FileReader("./day05/data/1.txt");
        //2. 创建输出流, 关联目的地文件.
        FileWriter fw = new FileWriter("./day05/data/3.txt");
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = fr.read()) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            fw.write(len);
        }
        //6. 释放资源.
        fr.close();
        fw.close();
    }
}
