package com.niudong.esdemo.Demo.IO.demo10_扩展知识_理解;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

//补充知识一: 设计模式.
/*
    理解:
        变量, 常量, if, switch, for -> 方法 -> 类 -> 父类 -> 接口 -> 设计模式 -> 框架, 架构.
    概述:
        实际开发中, 我们发现好多模块中的内容是相似的, 每次写很麻烦, 于是把这些相似的内容抽取出来, 定义成模型.
        这样按照模型做出来的东西就是符合某些特性或者具有某些功能, 这些模型就叫设计模式.
    大白话:
        设计模式就是解决问题的一系列思路, 它不属于任何的编程语言, 而是前辈们的开发经验总结, 是一系列的解决问题的思路和方案.
    分类:
        //设计模式一共有 23种, 分为3类.

        1. 创建型.         //5种
            特点: 需要创建对象.
            具体分类:
                建造者模式
                抽象工厂模式
                工厂方法模式
                单例模式
                原型模式

        2. 结构型          //7种
            特点: 用来描述类与类(接口)之间的关系.
            具体分类:
                装饰设计模式
                适配器设计模式
                外观模式
                享元模式
                组合模式
                桥接模式
                代理默认

        3. 行为型.         //11种
            特点: 事物能够做什么.
            具体分类:
                观察者模式
                访问者模式
                中介者模式
                解释器默认
                迭代器模式
                模板方法设计模式
                职责链模式
                备忘录模式
                命令模式
                状态模式
                策略模式.


    装饰设计模式:
        应用场景:
            就是用来对某个对象(类)的功能进行装饰(增强的).
        固定思路:
            1. 定义装饰类, 在装饰类中定义成员变量, 记录被装饰的对象的类型.
            2. 在构造方法中传入, 具体的要被装饰的对象.
            3. 对 被装饰对象的功能进行升级.
 */
//定义接口
interface Coder{        //程序员                           //InputStream
    void code();
}

//被装饰的类.
class Student implements Coder{                           //FileInputStream
    public void code() {
        System.out.println("Hello World!");
        System.out.println("变量, 常量");
        System.out.println("数组, 方法...");
    }

    public void smoke(){}
}

//装饰类
class HeimaStudent implements Coder {                   //BufferedInputStream
    //1. 定义装饰类, 在装饰类中定义成员变量, 记录被装饰的对象的类型.
    private Student s;

    //2. 在构造方法中传入, 具体的要被装饰的对象.
    public HeimaStudent(Student s) {
        this.s = s;
    }

    //对 被装饰对象的功能进行升级.
    @Override
    public void code() {
        s.code();
        System.out.println("来黑马以后, 学会了: JavaSE, JavaWeb");
        System.out.println("来黑马以后, 学会了: Hadoop生态圈, Zookeeper");
        System.out.println("来黑马以后, 学会了: 千亿级数据仓, 360°全方位用户画像");
    }
}

public class Demo01 {
    public static void main(String[] args) throws FileNotFoundException {
        //测试学生类中的方法
        Student xiaoLi = new Student();
        xiaoLi.code();
        System.out.println("--------------------------");

        //小张: 没来黑马以前.
        Student xz = new Student();
        xz.code();
        System.out.println("--------------------------");

        //小张: 来黑马以后.
        //HeimaStudent hs = new HeimaStudent(某一个学生对象);
        HeimaStudent hs = new HeimaStudent(xz);
        hs.code();
        //hs.smoke();

        //装饰类                                              被装饰的具体对象
        //HeimaStudent                                          Student xz
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("路径"));
    }
}
