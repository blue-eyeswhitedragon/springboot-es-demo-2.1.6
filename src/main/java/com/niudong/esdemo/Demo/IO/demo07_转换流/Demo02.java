package com.niudong.esdemo.Demo.IO.demo07_转换流;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

//案例: 演示转换输出流的 5种写数据的方式.
/*
    OutputStreamWriter的5个成员方法:
        public void write(int ch);
        public void write(char[] chs);
        public void write(char[] chs, int start, int len);
        public void write(String str);
        public void write(String str, int start, int len);
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //核心思想: 字符流 = 字节流 + 编码表.
        //1. 创建输出流对象, 关联目的地文件.
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("./day05/data/1.txt"));
        //2. 往目的地文件中写数据.
        //测试方法: public void write(int ch);
        //osw.write('夯');
        //osw.write('哥');
        //osw.flush();

        //测试方法: public void write(char[] chs);
        //char[] chs = {'我', '是', '最', '纯', '洁', '的'};
        //osw.write(chs);

        //测试方法: public void write(char[] chs, int start, int len);
        //osw.write(chs,3 ,2);

        //测试方法: public void write(String str);
        //osw.write("夯哥是最纯洁的!");

        //测试方法: public void write(String str, int start, int len);
        //3. 释放资源.
        //osw.write("糖糖是最邪恶的, 哈哈, 我刚才说的是反话!", 0, 6);
        osw.close();
    }
}
