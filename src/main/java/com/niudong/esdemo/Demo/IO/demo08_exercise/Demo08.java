package com.niudong.esdemo.Demo.IO.demo08_exercise;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

//案例: 点名器
public class Demo08 {
    public static void main(String[] args) throws Exception {
        //需求: 我有一个文件里面存储了班级同学的姓名，每一个姓名占一行，要求通过程序实现随点名器
        //1. 创建集合对象, 用来存储字符串.
        ArrayList<String> list = new ArrayList<>();
        //2. 创建字符高效输入流, 用来读取数据源文件.
        BufferedReader br = new BufferedReader(new FileReader("./day05/data/3.txt"));
        //3. 定义变量, 记录读取到的数据(字符串)
        String line = null;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的数据赋值给变量.
        while ((line = br.readLine()) != null) {
            //5. 将读取到的数据写入到集合中.
            list.add(line);
        }
        //6. 关流.
        br.close();
        //7. 创建Random对象.
        Random r = new Random();
        //8. 获取一个随机数.
        int index = r.nextInt(list.size());     //加入传入10, 随机数的范围是: 0 ~ 9
        //9. 根据该随机数, 获取其在list集合中对应的元素.
        String name = list.get(index);
        //10. 打印结果.
        System.out.println(name);

        //System.out.println(list.get(new Random().nextInt(list.size())));
    }
}
