package com.niudong.esdemo.netty;

import java.util.concurrent.Executor;

public class HelloExecutor implements Executor {

    public static void main(String[] args) {

        HelloExecutor helloExecutor = new HelloExecutor();

        helloExecutor.execute(()-> System.out.println("Hello Executor！"));

    }

    @Override

    public void execute(Runnable command) {

        command.run();

    }

}