package com.niudong.esdemo.lucene;

import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexableFieldType;

public class MyselfField extends Field {
    protected MyselfField(String name, IndexableFieldType type) {
        super(name, type);
    }
}
