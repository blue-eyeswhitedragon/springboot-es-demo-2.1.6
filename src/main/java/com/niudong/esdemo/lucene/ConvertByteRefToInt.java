package com.niudong.esdemo.lucene;

import org.apache.lucene.util.BytesRef;

public class ConvertByteRefToInt {
  public static void decode(BytesRef bytesRef){
    byte[] bytes = bytesRef.bytes;
    int end = bytesRef.offset + bytesRef.length;
    int ord = 0;
    int offset = bytesRef.offset;
    int prev = 0;
    while (offset < end) {
      byte b = bytes[offset++];
      // if语句为真：byte字节的最高位是0，decode结束
      if (b >= 0) {
        // ord的值为差值，所以(真实值 = 差值(ord) + 前面一个值(prev))
        prev = ord = ((ord << 7) | b) + prev;
        // 输出结果
        System.out.println(ord);
        ord = 0;
        // decode没有结束，需要继续拼接
      } else {
        // 每次处理一个byte
        ord = (ord << 7) | (b & 0x7F);
      }
    }
  }
  public static void main(String[] args) {
    int[] array = {3, 2, 2, 8, 12};
    // 去重编码
    BytesRef ref = ConvertIntToByteRef.dedupAndEncode(array);
    // 解码
    ConvertByteRefToInt.decode(ref);
  }
}