package com.niudong.esdemo.lucene;

import java.io.*;

public class Demo08 {
    public static void main(String[] args) throws Exception {
        //获取当前时间的毫秒值.
        long start = System.currentTimeMillis();


        method();                                          //28毫秒

        long end = System.currentTimeMillis();
        System.out.println("程序执行了: " + (end - start) + "毫秒");
    }

    public static void method() throws IOException {
        //1. 创建输入流, 关联数据源文件.
        FileInputStream fis = new FileInputStream("demo05/data/1.txt");
        BufferedInputStream bis = new BufferedInputStream(fis);
        //2. 创建输出流, 关联目的地文件.
        FileOutputStream fos = new FileOutputStream("demo05/data/2.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).
        int len = 0;
        byte[] bys = new byte[1024];
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((len = bis.read(bys)) != -1) {
            //5. 将读取到的数据写入到 指定的目的地文件中.
            bos.write(bys, 0, len);
        }
        //6. 释放资源.
        bis.close();
        bos.close();
    }

}
