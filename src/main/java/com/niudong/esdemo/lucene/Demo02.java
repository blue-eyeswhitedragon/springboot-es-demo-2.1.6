package com.niudong.esdemo.lucene;

import org.apache.lucene.codecs.compressing.CompressionMode;
import org.apache.lucene.codecs.compressing.Compressor;
import org.apache.lucene.store.ByteArrayDataOutput;

import java.io.FileInputStream;
import java.io.FileOutputStream;

//案例: 字节输入流一次读取一个字节数组.
/*
    FileInputStream:
        概述:
            表示普通的字节输入流, 它是一个具体的类.
        成员方法:
            public int read();             从数据源文件中读取数据, 一次读取一个字节. 并返回该字节的ASCII码值, 读不到返回-1.
            public int read(byte[] bys)    从数据源文件中读取数据, 一次读取一个字节数组. 并将读取到的内容存储到字节数组中,
                                           然后返回 读取到的有效字节数, 读不到返回-1.
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1. 创建字节输入流对象, 关联数据源文件.
        FileInputStream fis = new FileInputStream("demo05/data/1.txt");
        FileOutputStream fos = new FileOutputStream("demo05/data/3.txt");
        //2.1 定义直接数组用来存储读取到的内容(字节)
        byte[] bys2 = new byte[1024];
        //2.2 定义变量, 记录读取到的有效字节数.
        int len = 0;

        Compressor compressor = CompressionMode.HIGH_COMPRESSION.newCompressor();

        byte[] outPutBytes = new byte[1024];
        ByteArrayDataOutput dataOutput = new ByteArrayDataOutput(outPutBytes);

        //2.3 循环读取, 只要条件满足, 就一直读, 并将读取到的有效字节数赋值给变量.
        while ((len = fis.read(bys2)) != -1) {

            compressor.compress(bys2,0,len,dataOutput);

            fos.write(outPutBytes,0,dataOutput.getPosition());

            fos.write("\r\n".getBytes());
        }

        //3. 释放资源.
        fos.close();
        fis.close();
    }
}
