package com.niudong.esdemo.lucene;

import org.apache.lucene.codecs.compressing.CompressionMode;
import org.apache.lucene.codecs.compressing.Compressor;
import org.apache.lucene.store.ByteArrayDataOutput;

import java.io.*;


public class Demo05IO2 {
    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader("demo05/data/2.txt"));
        FileOutputStream fos = new FileOutputStream("demo05/data/3.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter("demo05/data/3.txt"));
        //3. 定义变量, 记录读取到的内容(字节, 有效字节数, 字符, 有效字符数, 字符串).

        Compressor compressor = CompressionMode.HIGH_COMPRESSION.newCompressor();

        byte[] outPutBytes = new byte[8024];
        ByteArrayDataOutput dataOutput = new ByteArrayDataOutput(outPutBytes);
        String line = null;
        //4. 循环读取, 只要条件满足就一直读, 并将读取到的内容赋值给变量.
        while ((line = br.readLine()) != null) {
            //5. 将读取到的数据写入到 指定的目的地文件中.

            System.out.println(line.getBytes().length);


            byte[] bytes = line.getBytes("UTF-8");
            compressor.compress(bytes,0,bytes.length,dataOutput);

//
//            bw.append(outPutBytes.toString());


            //bw.newLine();       //根据操作系统给出对应的换行符.


            fos.write(outPutBytes,0,dataOutput.getPosition());
            System.out.println(dataOutput.getPosition());

            fos.write("\r\n".getBytes());

            dataOutput.reset(outPutBytes);
        }
        //6. 释放资源.
        br.close();
        bw.close();
    }
}
