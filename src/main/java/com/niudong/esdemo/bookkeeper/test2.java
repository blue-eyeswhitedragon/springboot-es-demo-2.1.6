package com.niudong.esdemo.bookkeeper;

import org.apache.bookkeeper.client.BKException;
import org.apache.bookkeeper.client.BookKeeper;

import java.io.IOException;

public class test2 {
    public static void main(String[] args) {
        // 第一种初始化 BookKeeper Client 的方法
        try {
            String connectionString = "192.168.1.70:4181";


            // For a single-node, local ZooKeeper cluster
            BookKeeper bkClient = new BookKeeper(connectionString);
        } catch (InterruptedException | IOException | BKException e) {
            e.printStackTrace();
            throw new RuntimeException("There is an exception throw while creating the BookKeeper client.");
        }


    }
}
