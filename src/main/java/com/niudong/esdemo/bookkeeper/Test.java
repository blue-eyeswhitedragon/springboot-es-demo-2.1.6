package com.niudong.esdemo.bookkeeper;

import org.apache.bookkeeper.client.BKException;
import org.apache.bookkeeper.client.BookKeeper;
import org.apache.bookkeeper.client.LedgerHandle;
import org.apache.zookeeper.KeeperException;

import java.io.IOException;

public class Test {
    public static void main(String[] args) {
        try {
            String connectionString = "192.168.1.237:2181"; // For a single-node, local ZooKeeper cluster
            BookKeeper bkClient = new BookKeeper(connectionString);


            byte[] password = "some-password".getBytes();
            LedgerHandle handle = bkClient.createLedger(BookKeeper.DigestType.MAC, password);

            handle.close();
        } catch (InterruptedException | IOException | BKException e) {
            e.printStackTrace();
        }
    }
}
