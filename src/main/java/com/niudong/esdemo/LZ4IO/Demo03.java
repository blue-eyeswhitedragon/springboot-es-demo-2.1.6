package com.niudong.esdemo.LZ4IO;

import org.apache.lucene.codecs.compressing.CompressionMode;
import org.apache.lucene.codecs.compressing.Compressor;
import org.apache.lucene.codecs.compressing.Decompressor;
import org.apache.lucene.store.ByteArrayDataInput;
import org.apache.lucene.store.ByteArrayDataOutput;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;

public class Demo03 {
    public static void main(String[] args) throws Exception {

        String s1 = "2020-10-20 09:49:33,62立刻搭街坊s [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"ytgtqrs mxmkvmdr pcx rfvzh iviesyjkvk nhzrel gvvo.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.558\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573558,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,630 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"zbpgi ssw tvvc xfkculdn cpnmrr inql tgoxwogzr.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.630\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573630,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,640 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"dwecq zamitii munwlfq ilihdzd dgafackcc dmhgh flbw.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.640\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573640,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,640 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"wwrhzrj oipcdw xbrvw etxyyoho lxza glvspvx gfz.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.640\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573640,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"jywgk oaucnbywy pltpe isrdxewca sijzbldss itcatmv hpqackuuxs.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"hoyxagh agvrleihmv lrmzoi kmyrphvwa oroj vfa bozdz.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"aha xllg otadhf kobkd jvkp kzpvp uda.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,642 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"sdacoygl gjm aplzrlob ehliwpe lmkh eeh awmf.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
                + "2020-10-20 09:49:33,642 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"lbqmafury ybtpqby nsmtbnuzud kelo kzjomvgpjv ysba crkqlawc.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.642\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573642,\"tx\":33}\r\n"
                + "";

        String s2 = "开始觉得发货很快就收到回复收到了开个会立刻收到回复看脸时代快疯了金黄色的考虑房价凉快圣诞节弗兰克";


        String s3 ="大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大asd数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数ad据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数ds据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数2312312据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据\n" +
                "大数据大数据大数据大数据大数据\n" +
                "大数据";

        System.out.println("---------------------------------------");


       // test(s1);

        System.out.println("---------------------------------------");
       // test(s2);

        System.out.println("---------------------------------------");
        test(s3);

    }

    private static void test(String s) throws IOException {
        Compressor compressor = CompressionMode.HIGH_COMPRESSION.newCompressor();
        Decompressor decompressor = CompressionMode.HIGH_COMPRESSION.newDecompressor();
        byte[] bytes = s.getBytes("UTF-8");
        System.out.println("字符串长度：" + s.length());
        System.out.println("字符串byte长度：" + bytes.length);
        byte[] outPutBytes = new byte[1024];
        ByteArrayDataOutput dataOutput = new ByteArrayDataOutput(outPutBytes);
        ByteArrayDataInput dataInput = new ByteArrayDataInput(outPutBytes);
        compressor.compress(bytes,0,bytes.length,dataOutput);
        System.out.println("压缩后：" + dataOutput.getPosition());
        BytesRef ref = new BytesRef(1024);
        //decompressor.decompress(dataInput,bytes.length,0,bytes.length,ref);
        decompressor.decompress(dataInput,bytes.length,0,bytes.length,ref);
        System.out.println("解压字符串后：：" + ref.utf8ToString().length());
        System.out.println(ref.utf8ToString());
    }

}
