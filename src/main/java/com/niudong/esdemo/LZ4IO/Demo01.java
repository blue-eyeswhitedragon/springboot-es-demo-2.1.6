package com.niudong.esdemo.LZ4IO;

import net.jpountz.lz4.*;
import org.apache.lucene.codecs.compressing.CompressionMode;
import org.apache.lucene.codecs.compressing.Compressor;
import org.apache.lucene.codecs.compressing.Decompressor;
import org.apache.lucene.store.ByteArrayDataInput;
import org.apache.lucene.store.ByteArrayDataOutput;
import org.apache.lucene.util.BytesRef;
import org.junit.Test;

import java.io.*;


public class Demo01 {

    final int BLOCK_64K = 64 * 1024;
    final int BLOCK_128K = 128 * 1024;
    final int MAX_BLOCK_SIZE = 32 * 1024 * 1024;
    @Test
    public void test() throws Exception{
        InputStream inputStream = new BufferedInputStream(new FileInputStream("demo05/data/3.txt"));
        OutputStream outputStream =new BufferedOutputStream( new FileOutputStream( "demo05/data/5.txt" ) );

        LZ4FastDecompressor fastDecompressor = LZ4Factory.nativeInstance().fastDecompressor();
        //LZ4Compressor fastCompressor = LZ4Factory.nativeInstance().fastCompressor();

        //LZ4BlockOutputStream lz4BlockOutputStream = new LZ4BlockOutputStream(new LZ4BlockOutputStream(outputStream, BLOCK_64K, fastCompressor), BLOCK_64K, fastCompressor);
        LZ4BlockInputStream lz4BlockInputStream = new LZ4BlockInputStream(inputStream, fastDecompressor);

        int len = 0;
        byte[] bys = new byte[1024];
        while ( (len = lz4BlockInputStream.read(bys)) != -1) {
            //3. 将上述读取到的数据打印到控制台上.
            System.out.println(len);
            outputStream.write(bys, 0, len);
            outputStream.flush();
        }
        outputStream.close();
        lz4BlockInputStream.close();

    }
    @Test
    public void decompress02()throws Exception{

        InputStream inputStream = new BufferedInputStream(new FileInputStream("demo05/data/3.txt"));
        LZ4FastDecompressor fastDecompressor = LZ4Factory.nativeInstance().fastDecompressor();
        LZ4BlockInputStream lz4BlockInputStream = new LZ4BlockInputStream(inputStream, fastDecompressor);
        BufferedReader br = new BufferedReader(new FileReader("demo05/data/3.txt"));
        BufferedWriter bw   =new BufferedWriter(new FileWriter("demo05/data/4.txt"));
        Decompressor decompressor = CompressionMode.HIGH_COMPRESSION.newDecompressor();
        byte[] outPutBytes = new byte[8196];
        ByteArrayDataInput dataInput = new ByteArrayDataInput(outPutBytes);
        String line = null;
        while ((line = br.readLine()) != null) {
            System.out.println("字符：" +line.getBytes());
            System.out.println("字符串byte长度：" +line.getBytes().length);
            byte[] bytes = line.getBytes();

            BytesRef ref = new BytesRef(8196);
            decompressor.decompress(dataInput,bytes.length,0,bytes.length,ref);
            System.out.println("解压字符串后：：" + ref.utf8ToString().length());

            bw.write(ref.utf8ToString());

            bw.newLine();
        }
        //6. 释放资源.
        bw.close();
        br.close();
    }
    @Test
    public void compress02()throws Exception{
        BufferedReader br = new BufferedReader(new FileReader("demo05/data/1.txt"));
        FileOutputStream fos = new FileOutputStream("demo05/data/3.txt");

        LZ4Compressor fastCompressor = LZ4Factory.nativeInstance().fastCompressor();
        LZ4BlockOutputStream lz4BlockOutputStream = new LZ4BlockOutputStream(new LZ4BlockOutputStream(fos, BLOCK_64K, fastCompressor), BLOCK_64K, fastCompressor);


        String line = null;
        while ((line = br.readLine()) != null) {
            //System.out.println(line);
            lz4BlockOutputStream.write(line.getBytes());
            lz4BlockOutputStream.flush();
        }
        // LZ4Factory.fastestInstance().
        //6. 释放资源.
        fos.close();
        br.close();
    }

    @Test
    public void compress()throws Exception{
        BufferedReader br = new BufferedReader(new FileReader("demo05/data/1.txt"));
        FileOutputStream fos = new FileOutputStream("demo05/data/3.txt");
        Compressor compressor = CompressionMode.HIGH_COMPRESSION.newCompressor();
        byte[] outPutBytes = new byte[8196];
        ByteArrayDataOutput dataOutput = new ByteArrayDataOutput(outPutBytes);
        String line = null;
        while ((line = br.readLine()) != null) {
            System.out.println("字符串byte长度：" +line.getBytes().length);
            byte[] bytes = line.getBytes();
            compressor.compress(bytes,0,bytes.length,dataOutput);
            fos.write(outPutBytes,0,dataOutput.getPosition());
            System.out.println("压缩后：" + dataOutput.getPosition());
            fos.write("\r\n".getBytes());
            dataOutput.reset(outPutBytes);
        }
       // LZ4Factory.fastestInstance().
        //6. 释放资源.
        fos.close();
        br.close();
    }

    @Test
    public void decompress()throws Exception{
        BufferedReader br = new BufferedReader(new FileReader("demo05/data/3.txt"));
        BufferedWriter bw   =new BufferedWriter(new FileWriter("demo05/data/4.txt"));
        Decompressor decompressor = CompressionMode.HIGH_COMPRESSION.newDecompressor();
        byte[] outPutBytes = new byte[8196];
        ByteArrayDataInput dataInput = new ByteArrayDataInput(outPutBytes);
        String line = null;
        while ((line = br.readLine()) != null) {
            System.out.println("字符：" +line.getBytes());
            System.out.println("字符串byte长度：" +line.getBytes().length);
            byte[] bytes = line.getBytes();

            BytesRef ref = new BytesRef(8196);
            decompressor.decompress(dataInput,bytes.length,0,bytes.length,ref);
            System.out.println("解压字符串后：：" + ref.utf8ToString().length());

            bw.write(ref.utf8ToString());

            bw.newLine();
        }
        //6. 释放资源.
        bw.close();
        br.close();
    }

}
