package com.niudong.esdemo.LZ4IO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZIPUtils {
	public static final String GZIP_ENCODE_UTF_8 = "UTF-8";
	public static final String GZIP_ENCODE_ISO_8859_1 = "ISO-8859-1";

	public static byte[] compress(String str, String encoding) {
		if (str == null || str.length() == 0) {
			return null;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GZIPOutputStream gzip;
		try {
			gzip = new GZIPOutputStream(out);
			gzip.write(str.getBytes(encoding));
			gzip.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out.toByteArray();
	}

	public static byte[] compress(String str) throws IOException {
		return compress(str, GZIP_ENCODE_UTF_8);
	}

	public static byte[] uncompress(byte[] bytes) {
		if (bytes == null || bytes.length == 0) {
			return null;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		try {
			GZIPInputStream ungzip = new GZIPInputStream(in);
			byte[] buffer = new byte[1024];
			int n;
			while ((n = ungzip.read(buffer)) >= 0) {
				out.write(buffer, 0, n);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out.toByteArray();
	}

	public static String uncompressToString(byte[] bytes, String encoding) {
		if (bytes == null || bytes.length == 0) {
			return null;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		try {
			GZIPInputStream ungzip = new GZIPInputStream(in);
			byte[] buffer = new byte[256];
			int n;
			while ((n = ungzip.read(buffer)) >= 0) {
				out.write(buffer, 0, n);
			}
			return out.toString(encoding);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String uncompressToString(byte[] bytes) {
		return uncompressToString(bytes, GZIP_ENCODE_UTF_8);
	}

	public static void main(String[] args) throws IOException {
		String s1 = "2020-10-20 09:49:33,62立刻搭街坊s [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"ytgtqrs mxmkvmdr pcx rfvzh iviesyjkvk nhzrel gvvo.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.558\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573558,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,630 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"zbpgi ssw tvvc xfkculdn cpnmrr inql tgoxwogzr.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.630\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573630,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,640 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"dwecq zamitii munwlfq ilihdzd dgafackcc dmhgh flbw.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.640\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573640,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,640 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"wwrhzrj oipcdw xbrvw etxyyoho lxza glvspvx gfz.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.640\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573640,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"jywgk oaucnbywy pltpe isrdxewca sijzbldss itcatmv hpqackuuxs.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"hoyxagh agvrleihmv lrmzoi kmyrphvwa oroj vfa bozdz.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,641 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"aha xllg otadhf kobkd jvkp kzpvp uda.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,642 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"sdacoygl gjm aplzrlob ehliwpe lmkh eeh awmf.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.641\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573641,\"tx\":33}\r\n"
				+ "2020-10-20 09:49:33,642 [INFO] [main] com.test.es.ElasticSearchUtils [ElasticSearchUtils.java : 132] {\"collectd_type\":\"if_errors\",\"engine\":\"lbqmafury ybtpqby nsmtbnuzud kelo kzjomvgpjv ysba crkqlawc.\",\"host\":\"arcana-dev\",\"@meta\":{\"date_hour\":9,\"date_mday\":2,\"date_minute\":49,\"date_month\":9,\"date_second\":33,\"date_wday\":0,\"host\":\"arcana-dev\",\"source\":\"collectd\",\"time\":\"2020-10-20 09:49:33.642\"},\"plugin\":\"interface\",\"plugin_instance\":\"lo\",\"rx\":49,\"timestamp\":1603158573642,\"tx\":33}\r\n"
				+ "";

		String s = "开始觉得发货很快就收到回复收到了开个会立刻收到回复看脸时代快疯了金黄色的考虑房价凉快圣诞节弗兰克";

		System.out.println("字符串长度：" + s1.length());
		System.out.println("字符串byte长度：" + s1.getBytes().length);
		System.out.println("压缩后：：" + compress(s1).length);
		System.out.println("解压后：" + uncompress(compress(s1)).length);
		System.out.println("解压字符串后：：" + uncompressToString(compress(s1)).length());
//		System.out.println("解压字符串后：：" + uncompressToString(compress(s)));

		System.out.println("------------------------------------------");
		System.out.println("字符串长度：" + s.length());
		System.out.println("字符串byte长度：" + s.getBytes().length);
		System.out.println("压缩后：：" + compress(s).length);
		System.out.println("解压后：" + uncompress(compress(s)).length);
		System.out.println("解压字符串后：：" + uncompressToString(compress(s)).length());
		System.out.println("解压字符串后：：" + uncompressToString(compress(s)));
	}
}